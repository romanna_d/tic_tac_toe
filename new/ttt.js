




//var activePlayer = 1; // 1 - хід Х (дефолтне), -1 - хід О
var opponent = "man"; //"comp" - грає комп'ютер (дефолтне), "man" - грає людина

var scoreX = 0;
var scoreO = 0;
var gameOver = 1;//0-гра триває, 1 - гра закінчена



//функція-конструктор ігрової матриці

var Matrix = function(X, Y) {	
	 var arr = [];
	 for(var i=0; i<=X; i++){
	    arr[i] = [];
	    for(var j=0; j<=Y; j++){
	      arr[i][j] = 0;
	    };
	 };		
	this.deskArray = arr;
	this.X=X;
	this.Y=Y;
	this.activePlayer = 1;	
};

//метод (для Matrix) визначення чи матриця заповнена (true - заповнена повністю , false - є пусті комірки)
Matrix.prototype.isMatrixFull = function(){
	for (var i=0; i <= this.X; i++) {
		for(var j =0; j <= this.Y; j++){
			if (this.deskArray[i][j]==0) {
				return false;
			};
		};
	};
	return true;
};

//метод (для Matrix) визначення чи комірка пуста (true - пуста , false - заповнена)
//arr -масив  індексів комірки в матриці
Matrix.prototype.isCellEmpty = function(arr){
	var activeCell = this.deskArray[arr[0]][arr[1]];
	return  (activeCell==0) ? true : false;
}
	
//метод (для Controler) визначення індексів активної комірки по заданому id (повертає масив з двох індексів)
function activeIndexes(ID) {
	var index = [];
	index.push(Math.floor(ID/10));
	for (var j = 0; j<=gameField.Y; j++){
		if (ID==(index[0] * 10)+j) {
			index.push(j);
		}
	};
	return index;
};

//метод (для Controler) визначення id активної комірки по заданих індексах (повертає стрічку)
function activeID(arr){
	if(arr[0]==0){
		return "0" + arr[1];
	} else {
		var tempID = 10*arr[0]+arr[1]
		return String(tempID);
	}
};

//метод (для Controler) змінити рахунок гри
function changeScore(arr){
	if(arr[2]=="X") {scoreX+=1;};
	if(arr[2]=="O") {scoreO+=1;};
};

//метод для (Controler) обнулити рахунок гри
function clearScore(){
	scoreX=0;
	scoreO=0;
	drawScore("X");
	drawScore("O");
};


//метод (для Matrix) обнулення матриці (і відновлення дефолтного activePlayer)
Matrix.prototype.clearMatrix= function(){
	for(var i=0; i<=this.X; i++){
		for (var j=0; j<=this.Y; j++){
			this.deskArray[i][j] = 0;
		};
	};
	this.activePlayer=1;
};


//метод (для Matrix) заповнення комірки в матриці (повертає заповнений символ)
Matrix.prototype.writeIntoEmptyCell = function(arr){
	
	if (this.activePlayer == 1) {
		this.deskArray[arr[0]][arr[1]] = "X"
	} else {
		this.deskArray[arr[0]][arr[1]] = "O"
	};	
	return this.deskArray[arr[0]][arr[1]];
};

//метод для (Matrix) зміна активного гравця
 Matrix.prototype.changeActivePlayer = function(){
 	return this.activePlayer *= -1;
 };

 //метод для (Matrix) визначення переможного рядка
 //повертає масив із 3-х значень: 
 //1 - напрямок заповнення(1-рядок; 2-колонка; 3-перша діагональ; 4-друга діагональ)
 //2 - номер рядка або колонки переможця (у випадку діагоналей - 0)
 //3 - "Х" або "O"
 //якщо переможця нема, повертає [0, 0, 0]

 Matrix.prototype.getWinnerRow = function(){
 	
 	var winInform = [0, 0, 0];
 	
 	var temp = 0;
 	var i;
 	var j;
 	//для рядків
 	for ( i=0; i<=this.X; i++) {
 		temp = 0;
 		if(this.deskArray[i][0] !==0){
 			for (j = 0; j <=this.Y-1; j++){
 				if(this.deskArray[i][j] !== this.deskArray[i][j+1]) {
 					temp = 1;
 				};
 			};
 			if (temp == 0) {
 				winInform[0] = 1;
 				winInform[1] = i;
 				winInform[2] = this.deskArray[i][0];
 				return winInform;
 			};
 		};
 	};
 	//для колонок
 	for (j = 0; j<=this.Y; j++) {
 		 temp = 0;
 		if(this.deskArray[0][j]!==0){
 			for (i = 0; i <= this.X-1; i++) { 				
 				if(this.deskArray[i][j] !== this.deskArray[i+1][j]) {
 					temp = 1; 					
 				};
 			};
 			
 			if (temp == 0) {
 				winInform[0] = 2;
 				winInform[1] = j;
 				winInform[2] = this.deskArray[0][j];
 				return winInform;
 			}
 		};
  	};
 	//для діагоналі 1 (i=j)
 	for (i = 0; i <= this.X; i++) {
 		temp = 0;
 		if(this.deskArray[0][0]!==0) {
 			for (i = 0; i <= this.X-1; i++) {
 				j=i;
 				if(this.deskArray[i][j] !== this.deskArray[i+1][j+1]) {
 					temp = 1;
 				};
 			};
 			if (temp == 0) {
 				winInform[0] = 3;
 				winInform[1] = 0;
 				winInform[2] = this.deskArray[0][0];
 				return winInform;
 			};
 		};
 	};
 	//для діагоналі 2 (i+ J-)
 	for (i = 0; i <= this.X; i++) {
 		temp = 0;
 		if(this.deskArray[0][this.Y]!==0) {
 			j = this.Y;
 			for (i = 0; i <= this.X-1; i++) {
 				if(this.deskArray[i][j] !== this.deskArray[i+1][j-1]) {
 					temp = 1;
 				};
 				j=j-1;
 			};
 			if (temp == 0) {
 				winInform[0] = 4;
 				winInform[1] = 0;
 				winInform[2] = this.deskArray[0][this.Y];
 				return winInform;
 			};
 		};
 	};
return [0, 0, 0];

};//кінець функції getWinnerRow



//метод для (Matrix)??? перевірка стану матриці 
/*повертає масив із 3-х компонентів
arr[0] - 0 - переможця немає; 1-4 є переможець; 5 - нічия
arr[1] - номер рядка або колонки переможця або 0, якщо такогo номера нема
arr[2] - "активний" символ
*/
function matrixState(arr){
	var activeSymbol = gameField.writeIntoEmptyCell(arr);	
	var winner = gameField.getWinnerRow();
	//console.log(winner);
	winner[2]= activeSymbol;	
	if(winner[0] !== 0) {
		return winner;
	};
	//winner[2]= activeSymbol;
	if (gameField.isMatrixFull() ) { //нічия
		winner[0] = 5; 
		return winner;
	};	
	return winner;
};

//Функції Vewer
//намалювати символ (arg) в комірці (ID) кольором (signColor)
function drawXorO (ID, arg, signColor){
	$("#"+ID).text(arg)
			.css("font-size", "60px").
			css("color", signColor);
};

//намалювати переможний рядок (хрестики або нулики)
function drawWinnerString(arr){
	var indArr=[];
	if(arr[0]==1){		
		for(var j=0; j<=gameField.Y; j++){			
			var newID = activeID([arr[1], j]);			
			drawXorO(newID, arr[2], "red");
		};
	return;
	};

	if(arr[0] == 2) {
		for(var i=0; i<=gameField.X; i++){
			newID = activeID([i, arr[1]]);
			drawXorO(newID, arr[2], "red");
		};
		return;
	};

	if(arr[0] == 3) {
		for(i=0; i<=gameField.X; i++) {
			newID = activeID([i, i]);
			drawXorO(newID, arr[2], "red");
		};
		return;
	};

	if(arr[0] == 4) {
		j = gameField.Y;
		for(i=0; i<=gameField.X; i++) {
			newID = activeID([i, j]);
			drawXorO(newID, arr[2], "red");
			j--;
		};
		return;
	};
};

function clearDesk(){
	for(var i=0; i<=gameField.X; i++) {
		for(var j=0; j<=gameField.Y; j++){			
			var newID = activeID([i, j]);			
			drawXorO(newID, "", "#cccccc");
		};
	};
};

//намалювати інформацію про партнера
function drawPartner(partner){
	if (partner == "man" ){
		$(".stringInform").text("гра з партнером")			
			.css("color", "#cccccc");
	} else {
		$(".stringInform").text("гра з комп'ютером")			
			.css("color", "#cccccc");
	};
};

//намалювати стрічку з привітанням
function drawCongratulations(arr){	
	if (arr[0] == 5) {
		$(".stringInform").text("НІЧИЯ !!!")			
			.css("color", "red");			
	} else{
	$(".stringInform").text(function(){
					return "Переможець " + arr[2] + " !!!";
					})						
					.css("color", "red");
	};	
};


//намалювати новий рахунок гри
function drawScore(player){	
	
	$(".tdScoreX").text(function(){
			return scoreX;
			})				
			.css("color", "red");	
	
	$(".tdScoreO").text(function(){
			return scoreO;
			})				
			.css("color", "red");	
			
};




//створюємо ігрову матрицю
var gameField = new Matrix(2, 2);

//реакція на клік (по ігровому полю)
function clickCell (cellID) {
	if(gameOver == 1) {return};	
	var indexArr = activeIndexes(cellID);//індекси натиснутої комірки	
	if(!gameField.isCellEmpty(indexArr)) { return;};	
	var modelInformation = matrixState(indexArr);	
	drawXorO(cellID, modelInformation[2], "#34496e");
	switch (modelInformation[0]) {
		case 0:   //черговий хід		
			var player = gameField.changeActivePlayer();		
		break;

		case 1: //є рядок переможець
		case 2: //є колонка переможець
		case 3: //переможець діагональ (i=j)
		case 4: //переможець діагональ (i!+j)	
			drawWinnerString(modelInformation);//намалювати червоним кольором символи в рядку-переможці			
			changeScore(modelInformation);
			drawScore(modelInformation[2]);//в таблиці результатів
			drawCongratulations(modelInformation);
			gameOver = 1;
			$(".button").text("розпочати гру")
			.css("color", "red");
		break;		

		case 5: //нічия			
			drawScore(modelInformation[2]);
			console.log(modelInformation[0]);
			drawCongratulations(modelInformation);
			gameOver = 1;
			$(".button").text("розпочати гру")
			.css("color", "red");
		break;

	};
};	

	//метод для (Controler) зміни партнера "comp" або "man" (міняє глобальну змінну opponent )
function changeOpponent(radiobutton){
	if (gameOver == 0) {return;};
	if(radiobutton.value == "computer"){
		opponent = "comp";
	} else{
		opponent = "man";
	};
	clearScore();
	drawPartner(opponent);
	return opponent;
};

function startGame(startButton){
	if (gameOver == 0) {return};
	gameField.clearMatrix();	
	clearDesk();
	drawPartner(opponent);
	$(".button").text("гра триває")
	.css("color", "#cccccc");
	gameOver = 0;
};









